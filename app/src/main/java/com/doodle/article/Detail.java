package com.doodle.article;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


public class Detail extends AppCompatActivity {
    WebView webView;
    ProgressBar progres;
    TextView topic;

    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        webView = (WebView) findViewById(R.id.webview);
        topic = (TextView) findViewById(R.id.topic);
        progres = (ProgressBar) findViewById(R.id.progress);

        topic.setTextColor(0xFFFFFFFF);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(this, "HTMLOUT");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                webView.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progres.setProgress(progress);
                if (progress == 100) {
                    progres.setVisibility(View.GONE);
                }
            }
        });

        if (utils.checkInternetConnection()) {
            webView.loadUrl(getIntent().getStringExtra("name"));
        } else {
            progres.setVisibility(View.GONE);
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }


    }

    @JavascriptInterface
    public void processHTML(String html) {

        Document doc = Jsoup.parse(html);
        String title =
                doc.select("meta[name=title]").get(0)
                        .attr("content");
        Log.e("titleee", title);
        topic.setText(title);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }


}
