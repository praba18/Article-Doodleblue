package com.doodle.article;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

/**
 * Created by Praba-Crayond on 10/25/2017.
 */

public class utils {
    /* checking the intrenet connection */
    public static boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) MyApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) {
                /*Network connecting...*/
            return true;
        } else {
                /*oops!!! no network*/
            Log.d("TAG", "Internet Connection Not Present");
            return false;
        }
    }
}
