package com.doodle.article;

import android.app.Application;
import android.content.Context;



/**
 * Created by croyon11 on 10/24/2016.
 */
public class MyApplication extends Application {


    private static MyApplication sInstance;


    public static MyApplication getInstance() {
        return sInstance;
    }

    public static Context getAppContext() {
        return sInstance.getApplicationContext();
    }


    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);


    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

    }


}

