package com.doodle.article;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    WebView webView;
    ProgressBar progres;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = (WebView) findViewById(R.id.webview);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        progres = (ProgressBar) findViewById(R.id.progress);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progres.setProgress(progress);

                if (progress == 100) {
                    progres.setVisibility(View.GONE);
                    toolbar.setTitle(view.getTitle());
                }
            }
        });
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent i = new Intent(MainActivity.this, Detail.class);
                i.putExtra("name", url);
                startActivity(i);
                return true;
            }
        });
        if (utils.checkInternetConnection()) {
            webView.loadUrl("https://medium.com/");
        } else {
            progres.setVisibility(View.GONE);
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }
}
